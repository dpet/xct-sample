#!/bin/bash -e
echo "----------------------------------------------------------------------------"
echo "==> Preparing iPhone Simulators for tests..."
echo "----------------------------------------------------------------------------"
xcrun simctl shutdown all
# Erase all simulators contents and settings
xcrun simctl erase all
xcrun simctl boot "iPhone 11"
xcrun simctl spawn "iPhone 11" defaults write com.apple.springboard FBLaunchWatchdogScale 2
echo "----------------------------------------------------------------------------"
echo "==> Simulator is ready. Start tests on platform=iOS Simulator,name=iPhone 11,OS=latest in 2 parallel sim clones..."
echo "----------------------------------------------------------------------------"
xcodebuild -project RSSwift.xcodeproj -scheme Rsswift -destination 'platform=iOS Simulator,name=iPhone 11,OS=latest' -parallel-testing-worker-count 2 test clean
