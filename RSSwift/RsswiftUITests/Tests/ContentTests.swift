//
//  ContentTests.swift
//  RsswiftUITests
//
//  Created by pervyshev (Дмитрий Первышев) on 4/27/20.
//  Copyright © 2020 ArledKola. All rights reserved.
//

import XCTest

final class ContentTests: UITestCase {
    
    func test_checkCellsContent(){
        TestSteps(testCase: self).checkCellsContent()
    }
    
    func test_checkNewsMenu() {
        TestSteps(testCase: self).checkNewsMenu()
    }
    
    
}
