//
//  GeneralTests.swift
//  RsswiftUITests
//
//  Created by pervyshev (Дмитрий Первышев) on 4/27/20.
//  Copyright © 2020 ArledKola. All rights reserved.
//

import XCTest

final class GeneralTests: UITestCase {

    func test_checkButtons(){
        TestSteps(testCase: self).exampleCheck()
    }
    
    func test_suspendAndRelaunchApp(){
        TestSteps(testCase: self).suspendAndRelaunchApp()
    }
    
}
