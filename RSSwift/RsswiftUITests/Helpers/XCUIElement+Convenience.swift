//
//  XCUIElement+Convenience.swift
//  RsswiftUITests
//
//  Created by pervyshev (Дмитрий Первышев) on 4/27/20.
//  Copyright © 2020 ArledKola. All rights reserved.
//

import XCTest

extension XCUIElement {

    func button(_ str: String) -> XCUIElement {
        return self.buttons[str].firstMatch
    }
    
    func label(_ str: String) -> XCUIElement {
        return self.staticTexts[str].firstMatch
    }
    
    func navBar(_ str: String) -> XCUIElement {
        return self.navigationBars[str].firstMatch
    }
    func firstCell() -> XCUIElement {
        return self.cells.firstMatch
    }
    
    func alert(_ str: String) -> XCUIElement {
        return self.alerts[str].firstMatch
    }
    
    func waitToAppear(_ testCase: UITestCase, timeout: TimeInterval = 5, file: String = #file, line: Int = #line) {
        let pred = NSPredicate(format: "exists == true")
        let exp = XCTNSPredicateExpectation(predicate: pred, object: self)
        var message: String
        switch XCTWaiter().wait(for: [exp], timeout: timeout) {
        case .completed:
            print("Completed.")
        case .incorrectOrder:
            message = "An expectation for Element: '\(self.debugDescription)' has incorrect order."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        case .interrupted:
            message = "An expectation for Element: '\(self.debugDescription)' has interrupted."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        case .timedOut:
            message = "Element: '\(self.debugDescription)' is not appeared within \(timeout) seconds."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        case .invertedFulfillment:
            message = "An expectation for Element: '\(self.debugDescription)' has inverted fulfillment."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        @unknown default:
            message = "Unknown enum value."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        }
    }
    
    func waitToDisappear(_ testCase: UITestCase, timeout: TimeInterval = 5, file: String = #file, line: Int = #line) {
        let pred = NSPredicate(format: "exists == false")
        let exp = XCTNSPredicateExpectation(predicate: pred, object: self)
        var message: String
        
        switch XCTWaiter().wait(for: [exp], timeout: timeout) {
        case .completed:
            print("Completed")
        case .incorrectOrder:
            message = "An expectation for Element: '\(self.debugDescription)' has incorrect order."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        case .interrupted:
            message = "An expectation for Element: '\(self.debugDescription)' has interrupted."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        case .timedOut:
            message = "Element: '\(self.debugDescription)' is not didappeared within \(timeout) seconds."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        case .invertedFulfillment:
            message = "An expectation for Element: '\(self.debugDescription)' has inverted fulfillment."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        @unknown default:
            message = "Unknown enum value."
            testCase.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
        }
    }
    
    /**
     Checks element absence. Specify file and line parameters if you invoke this method from
     outside test method implementation.
     
     - parameter testCase: Reference to the running test case.
     - parameter file: The file path to the source file where the failure being reported was encountered.
     - parameter line: The line number in the source file at filePath where the failure being reported was encountered.
     *
     */
    func absence(_ testCase: UITestCase, file: String? = #file, line: Int? = #line) {
        if self.exists {
            let message = "Element '\(self)' doesn't absent"
            testCase.recordFailure(withDescription: message, inFile: file!, atLine: line!, expected: true)
        }
    }
    
    /**
     Checks element existence. Specify file and line parameters if you invoke this method from
     outside test method implementation.
     
     - parameter testCase: Reference to the running test case.
     - parameter file: The file path to the source file where the failure being reported was encountered.
     - parameter line: The line number in the source file at filePath where the failure being reported was encountered.
     *
     */
    func exists(_ testCase: UITestCase, file: String? = #file, line: Int? = #line) {
        if !self.exists {
            let message = "Element '\(self)' doesn't exist"
            testCase.recordFailure(withDescription: message, inFile: file!, atLine: line!, expected: true)
        }
    }
    
    func getCellTitle() -> String {
        return self.staticTexts.firstMatch.label
    }
    
    func getCellDate() -> String {
        return self.staticTexts.element(boundBy: self.staticTexts.count-1).label
    }
}
