//
//  TestSteps.swift
//  RsswiftUITests
//
//  Created by pervyshev (Дмитрий Первышев) on 4/27/20.
//  Copyright © 2020 ArledKola. All rights reserved.
//

import Foundation
import XCTest

@objc class TestSteps: NSObject, BaseSteps {
    
    var testCase: UITestCase
    
    required init(testCase: UITestCase) {
        self.testCase = testCase
        super.init()
    }
       
    func exampleCheck(){
//        testCase.printContent()
        let refresh = "Refresh"
        let feeds = "Feeds"
        app.button(refresh).exists(testCase)
        app.button(feeds).exists(testCase)
        tap(refresh)
        tap(feeds)
        
        app.navBar(feeds).waitToAppear(testCase)
        XCTAssertEqual("Empty list", app.tables.firstMatch.label)
        returnFrom(feeds)
        
    }
    
    func suspendAndRelaunchApp() {
        XCUIDevice.shared.press(.home)
        XCUIDevice.shared.press(.home)
        testCase.wait(1)
        // Go back to the app
        XCUIDevice.shared.press(.home)
        testCase.wait(1)
        // Suspend the app
        XCUIDevice.shared.press(.home)
        testCase.wait(1)
        // Relaunch the app
        // Note! This will terminate the existing process and launch the app freshly.
        // Tests do not terminate though.
        self.app.launch()
        testCase.printContent()
        testCase.wait(1)
    }

    func checkCellsContent() {
        let cells = app.cells
        for index in 1...cells.count {
            let cell = cells.element(boundBy: index-1)
            cell.tap()
            checkPrivacyPopUp()
            app.navBar("Feed Item").waitToAppear(testCase)
//            testCase.printContent()
            app.links.matching(identifier: "Open the menu").firstMatch.waitToAppear(testCase)
            
            app.links.matching(identifier: "Facebook").firstMatch.exists(testCase)
            app.links.matching(identifier: "Twitter").firstMatch.exists(testCase)
            app.links.matching(identifier: "YouTube").firstMatch.exists(testCase)
            app.links.matching(identifier: "Instagram").firstMatch.exists(testCase)
            returnFrom("Feed Item")
        }
    }
    
    func checkNewsMenu(){
        app.cells.firstMatch.tap()
        checkPrivacyPopUp()
        app.navBar("Feed Item").waitToAppear(testCase)
        app.links.matching(identifier: "Open the menu").firstMatch.tap()
        testCase.printContent()
        app.links.matching(identifier: "Home").firstMatch.exists(testCase)
        app.links.matching(identifier: "World").firstMatch.exists(testCase)
        app.links.matching(identifier: "Politics").firstMatch.exists(testCase)
        app.links.matching(identifier: "Climate").firstMatch.exists(testCase)
        app.links.matching(identifier: "Science & Tech").firstMatch.exists(testCase)
        app.links.matching(identifier: "Business").firstMatch.exists(testCase)
        app.links.matching(identifier: "Ents & Arts").firstMatch.exists(testCase)
        app.links.matching(identifier: "Travel").firstMatch.exists(testCase)
        app.links.matching(identifier: "Videos").firstMatch.exists(testCase)
        app.links.matching(identifier: "Weather").firstMatch.exists(testCase)
        returnFrom("Feed Item")
    }
    
    private func checkPrivacyPopUp() {
        testCase.wait(2)
        if app.label("privacy options").exists {
            app.button("Accept").waitToAppear(testCase)
            tap("Accept")
        }
        
    }
}
