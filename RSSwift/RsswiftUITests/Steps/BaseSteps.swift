//
//  BaseSteps.swift
//  RsswiftUITests
//
//  Created by pervyshev (Дмитрий Первышев) on 4/27/20.
//  Copyright © 2020 ArledKola. All rights reserved.
//
import XCTest

protocol BaseSteps {
    
    var testCase: UITestCase { get set }
    init(testCase: UITestCase)
}

extension BaseSteps {
    
    var app: XCUIApplication {
        return self.testCase.app
    }


    func tap(_ buttonLabel: String, file: String = #file, line: Int = #line) {
        let button = self.app.button(buttonLabel)
        button.waitToAppear(self.testCase, file: file, line: line)
        button.tap()
    }
    
    func returnFrom(_ navBarText: String, file: String? = #file, line: Int? = #line) {
        let navBar = app.navBar(navBarText)
        navBar.waitToAppear(testCase)
        let navBarButtons = navBar.children(matching: .button)
        if navBarButtons.count>0 {
            let returnButton = navBarButtons.firstMatch
            returnButton.exists(testCase, file: file, line: line)
            returnButton.tap()
        } else {
            XCTFail("No 'Back' and 'Close' buttons found")
        }
    }
    

}
